/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courstud;

import java.io.Console;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Date;
// Notice, do not import com.mysql.jdbc.*
// or you will have problems!
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoadDriver {

    public static void main(String[] args) {
        /* Create EntityManagerFactory */
        EntityManagerFactory emf = Persistence
                .createEntityManagerFactory("CourStudPU");

        User user = new User(3, "Ahmed", new Date(1990, 2, 15));
        user.setAddress("address");
        
        Course curs = new Course(2, "Meth", "Prof", 4);
        
        UserCourse uc = new UserCourse(1,user.getId(),curs.getId());
        
        /* Create EntityManager */
        EntityManager em = emf.createEntityManager();

        /* Persist entity */
        em.getTransaction().begin();
        em.persist(user);
        em.persist(curs);
        em.persist(uc);
        em.getTransaction().commit();
    }
}
